package com.ideazd.system.service;

import java.util.HashMap;
import java.util.Map;

import redis.clients.jedis.Jedis;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.redis.Redis;

public class TokenService {
	private static final Logger log = Logger.getLogger(TokenService.class);
	public static final TokenService me = new TokenService();
	
	public String getToken() {
		Jedis jedis = Redis.use().getJedis();
		String accessToken = jedis.get("ytv.accessToken");
		//检测是否有效   -- 无法检测
		if (StrKit.isBlank(accessToken)) {
			String accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + PropKit.get("ytvAppId") + "&secret="
					+ PropKit.get("ytvSecret");
			String accessTokenData = HttpKit.get(accessTokenUrl);
			log.debug("accessToken is refush "+accessTokenData);
			if (StrKit.notBlank(accessTokenData)) {
				Map accessTokenMap = JSON.parseObject(accessTokenData, Map.class);
				if (accessTokenMap!=null && accessTokenMap.containsKey("access_token")) {
					accessToken = accessTokenMap.get("access_token").toString();
					//Redis.use().setex("ytv.accessToken", 7000, accessToken);//该方法无效
					jedis.setex("ytv.accessToken", 7000, accessToken);
					/*暂时不存数据库
					try {
						WxAccessToken wxAccessToken = new WxAccessToken();
						wxAccessToken.set("access_token", accessToken);
						wxAccessToken.set("token_type", "user");
						wxAccessToken.set("create_time", new Date());
						wxAccessToken.set("expires_in", 7000);
						wxAccessToken.set("refresh_token", "");
						wxAccessToken.save();
					} catch (Exception e) {
						log.debug("save wxAccessToken is error " + e.getMessage());
					}
					*/
				} else accessToken = "";
			}
		} else {
			log.debug("UserAccessToken is use cache: "+accessToken);
		}
		jedis.close();
		return accessToken;
	}
}
