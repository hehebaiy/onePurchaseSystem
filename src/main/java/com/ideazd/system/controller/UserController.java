/**
 * 
 */
package com.ideazd.system.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.User;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年4月21日 下午4:00:40
 */
@ControllerBind(controllerKey="/user",viewPath="/pages")
@Before(AdminInterceptor.class)
public class UserController extends Controller {
	private static final Logger log = Logger.getLogger(UserController.class);
	
	
	//获取用户列表
	public void getUserList(){
		String curPageStr = getPara("curPage");
		String loginDate = getPara("loginDate");
		String nickName = getPara("nickName");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
        
        String sql = " FROM sta_user u LEFT JOIN  sta_login_log l ON u.`useid` = l.`useid` WHERE 1 = 1 and u.useid !='administrator' ";
        
        if(!StrKit.isBlank(nickName)){
        	sql += " and u.nickname like '%"+nickName+"%'";
        }
        
        if(!StrKit.isBlank(loginDate)){
        	sql += "and l.logtime >'"+loginDate+"' and l.logtime < "+ "'"+loginDate.substring(0,10)+" 23:59:59'";
        }
        
        Page<User> userPage = User.dao.paginate(curPage, pageSize, "SELECT DISTINCT(u.`useid`),u.*,(SELECT DISTINCT(logintime) FROM sta_login_log WHERE useid = u.`useid` ORDER BY logintime DESC) AS 'lastlogin' ", sql+" ORDER BY lastlogin DESC");
        List<Map<String, String>> userList = new ArrayList<Map<String, String>>();
        
        for(User u:userPage.getList()){
        	Map<String, String> user = new HashMap<String, String>();
        	user.put("id", u.getStr("useid"));
        	user.put("account", u.getStr("account"));
        	user.put("nickname", URLDecoder.decode(u.getStr("nickname").toString()));
        	user.put("gender", SysUtil.getDictionValue("administrator", "gender", u.getStr("gender")));
        	user.put("headportrait", u.getStr("headportrait"));
        	user.put("phone", u.getStr("phone"));
        	user.put("usetype", SysUtil.getDictionValue("sta_user", "usetype",u.getStr("usetype")));
        	user.put("logintime", u.get("logintime").toString().substring(0,u.get("logintime").toString().length()-2));
        	user.put("lastlogin", u.get("lastlogin").toString());
        	user.put("userstatus", u.getStr("userstatus"));
        	userList.add(user);
        }
        ret.put("data", userList);
        
        PagingUtil pagingUtil = new PagingUtil();
        pagingUtil.setRowCount(userPage.getTotalRow());
        pagingUtil.setCurrentPage(curPage);
        
        
        ret.put("pages", pagingUtil.getPageDisplay());
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 启用禁用
	 */
	public void stopAndEnableUser(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			
			User user = new User();
			user.set("useid", id);
			user.set("userstatus", status);
			
			if(user.update()){
				ret.setCode("0");
				ret.setMsg("成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
}
