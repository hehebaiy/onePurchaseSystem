/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.Commodity;
import com.ideazd.system.model.CommodityVersion;
import com.ideazd.system.model.CommodityVersionPic;
import com.ideazd.system.model.NewDetails;
import com.ideazd.system.model.OrderSummary;
import com.ideazd.system.model.Topic;
import com.ideazd.system.model.TopicComment;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/commodity",viewPath="/pages")
@Before(AdminInterceptor.class)
public class CommodityController extends Controller{
	
	/**
	 * 获取商品列表
	 */
	public void getCommodityList(){
		String curPageStr = getPara("curPage");
		String title = getPara("title");
		String publishDate = getPara("publishDate");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_commodity  where 1 = 1";
        
        if(!StrKit.isBlank(title)){
        	sql += " and commodity_name like '"+title+"'";
        }
        
        if(!StrKit.isBlank(publishDate)){
        	sql += " and createdate >'"+publishDate+"' and createdate < "+ "'"+publishDate.substring(0,10)+" 23:59:59'";
        }
		
		try {
			
			Page<Commodity> commodityPage = Commodity.dao.paginate(curPage, pageSize, "select *", sql+" order by sort desc");
	        List<Map<Object, Object>> commodityList = new ArrayList<Map<Object, Object>>();
	        
	        for(Commodity c:commodityPage.getList()){
	        	Map<Object, Object> commodity = new HashMap<Object, Object>();
	        	commodity.put("id", c.getStr("id"));
	        	commodity.put("commodityName",  c.getStr("commodity_name"));
	        	commodity.put("commodityTitle", c.getStr("commodity_title"));
	        	commodity.put("salesVolume", c.get("sales_volume").toString());
	        	commodity.put("commodityPicture", c.getStr("commodity_picture"));
	        	commodity.put("commodityPrice", c.get("commodity_price").toString());
	        	commodity.put("commodityStatus", c.get("commodity_status").toString());
	        	commodity.put("sort", c.get("sort").toString());
	        	commodity.put("createDate", c.get("createdate").toString().substring(0,c.get("createdate").toString().length()-2));
	        	commodity.put("createName", c.getStr("createname"));
	        	commodity.put("updateName", c.getStr("updatename"));
	        	commodityList.add(commodity);
	        }
	        ret.put("data", commodityList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(commodityPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	
	/**
	 * 添加商品
	 */
	public void addCmmodity(){
		File file = super.getFile("commodityPicture").getFile();
		String commodityName = getPara("commodityName");
		String commodityTitle = getPara("commodityTitle");
		String commodityPrice = getPara("commodityPrice");
		String commodityStatus = getPara("commodityStatus");
		String commodityDesc = getPara("commodityDesc");
		String sort = getPara("sort");
		RetDTO ret = new RetDTO();
		String commodityPicture = "";
		
		try {
			
			String uuid = SysUtil.getUUID();
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/commodity" + "/" + uuid + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				commodityPicture = imaPath.get("serverPath");

			}
			
			Administrator admin = getSessionAttr("admin");
			
			Commodity commodity = new Commodity();
			commodity.set("id", uuid);
			commodity.set("commodity_name", commodityName);
			commodity.set("commodity_title", commodityTitle);
			commodity.set("commodity_price", commodityPrice);
			commodity.set("commodity_status", commodityStatus);
			commodity.set("commodity_desc", commodityDesc);
			commodity.set("commodity_desc_url", getRequest().getScheme()+"://"+getRequest().getServerName()+":"+getRequest().getServerPort()+getRequest().getContextPath()+"/commodity/showCommodityDesc?id="+uuid);
			commodity.set("commodity_picture", commodityPicture);
			commodity.set("sort", sort);
			commodity.set("sales_volume", 0);
			commodity.set("createid", admin.get("id"));
			commodity.set("createname", admin.get("name"));
			commodity.set("createdate", new Date());
			commodity.set("updateid", admin.get("id"));
			commodity.set("updatename", admin.get("name"));
			commodity.set("updatedate", new Date());
			commodity.set("remarks", "添加商品");
			
			if(commodity.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑商品
	 */
	public void editCommodity(){
		File file = null;
		
		try {
			file = super.getFile("commodityPicture").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		
		String commodityName = getPara("commodityName");
		String commodityTitle = getPara("commodityTitle");
		String commodityPrice = getPara("commodityPrice");
		String commodityStatus = getPara("commodityStatus");
		String commodityDesc = getPara("commodityDesc");
		String id = getPara("id");
		String sort = getPara("sort");
		RetDTO ret = new RetDTO();
		String commodityPicture = "";
		
		
		
		try {
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/commodity" + "/" + id + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				commodityPicture = imaPath.get("serverPath");

			}
			
			Administrator admin = getSessionAttr("admin");
			
			Commodity commodity = new Commodity();
			commodity.set("id", id);
			commodity.set("commodity_name", commodityName);
			commodity.set("commodity_title", commodityTitle);
			commodity.set("commodity_price", commodityPrice);
			commodity.set("commodity_status", commodityStatus);
			commodity.set("commodity_desc", commodityDesc);
			
			if(!StrKit.isBlank(commodityPicture)){
				commodity.set("commodity_picture", commodityPicture);
			}
			
			commodity.set("sort", sort);
			commodity.set("updateid", admin.get("id"));
			commodity.set("updatename", admin.get("name"));
			commodity.set("updatedate", new Date());
			commodity.set("remarks", "修改商品");
			
			if(commodity.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除商品
	 */
	@SuppressWarnings("null")
	public void delCommodity(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_commodity where id = '"+id+"'");
				Db.update("delete from sta_commodity_version where commodity_id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据id获取商品
	 */
	public void findCommodityById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			Commodity commodity = Commodity.dao.findById(id);
        	ret.put("commodityName",  commodity.getStr("commodity_name"));
        	ret.put("commodityTitle", commodity.getStr("commodity_title"));
        	ret.put("commodityPicture", commodity.getStr("commodity_picture"));
        	ret.put("commodityPrice", commodity.get("commodity_price").toString());
        	ret.put("commodityStatus", commodity.get("commodity_status").toString());
        	ret.put("commodityDesc", commodity.get("commodity_desc").toString());
        	ret.put("sort", commodity.get("sort").toString());
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 *商品 上架/下架
	 */
	public void handleCommodityStatus(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			
			Commodity commodity = new Commodity();
			commodity.set("id", id);
			commodity.set("commodity_status", status);
			commodity.set("updateid", admin.get("id"));
			commodity.set("updatename", admin.get("name"));
			commodity.set("updatedate", new Date());
			commodity.set("remarks", "修改商品状态");
			
			if(commodity.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取商品型号列表
	 */
	public void getVersionList(){
		String curPageStr = getPara("curPage");
		String commodityId = getPara("commodityId");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_commodity_version  where commodity_id = '"+commodityId+"'";
		
		try {
			
			Page<CommodityVersion> commodityVersionPage = CommodityVersion.dao.paginate(curPage, pageSize, "select *", sql+" order by version_sort desc");
	        List<Map<Object, Object>> commodityVersionList = new ArrayList<Map<Object, Object>>();
	        
	        for(CommodityVersion c:commodityVersionPage.getList()){
	        	Map<Object, Object> commodityVersion = new HashMap<Object, Object>();
	        	commodityVersion.put("id", c.getStr("id"));
	        	commodityVersion.put("versionType",  c.getStr("version_type"));
	        	commodityVersion.put("versionPrice", c.get("version_price").toString());
	        	commodityVersion.put("versionStock", c.get("version_stock").toString());
	        	commodityVersion.put("versionSort", c.get("version_sort").toString());
	        	commodityVersionList.add(commodityVersion);
	        }
	        ret.put("data", commodityVersionList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(commodityVersionPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加商品型号
	 */
	public void addCmmodityVersion(){
		String versionType = getPara("versionType");
		String versionPrice = getPara("versionPrice");
		String versionStock = getPara("versionStock");
		String versionSort = getPara("versionSort");
		String commodityId = getPara("commodityId");
		RetDTO ret = new RetDTO();
		
		try {

			Administrator admin = getSessionAttr("admin");
			
			CommodityVersion commodityVersion = new CommodityVersion();
			commodityVersion.set("id", SysUtil.getUUID());
			commodityVersion.set("commodity_id", commodityId);
			commodityVersion.set("version_type", versionType);
			commodityVersion.set("version_price", versionPrice);
			commodityVersion.set("version_stock", versionStock);
			commodityVersion.set("version_sort", versionSort);
			commodityVersion.set("createid", admin.get("id"));
			commodityVersion.set("createname", admin.get("name"));
			commodityVersion.set("createdate", new Date());
			commodityVersion.set("updateid", admin.get("id"));
			commodityVersion.set("updatename", admin.get("name"));
			commodityVersion.set("updatedate", new Date());
			commodityVersion.set("remarks", "添加商品");
			
			if(commodityVersion.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑商品型号
	 */
	public void editCmmodityVersion(){
		String versionType = getPara("versionType");
		String versionPrice = getPara("versionPrice");
		String versionStock = getPara("versionStock");
		String versionSort = getPara("versionSort");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {

			Administrator admin = getSessionAttr("admin");
			
			CommodityVersion commodityVersion = new CommodityVersion();
			commodityVersion.set("id", id);
			commodityVersion.set("version_type", versionType);
			commodityVersion.set("version_price", versionPrice);
			commodityVersion.set("version_stock", versionStock);
			commodityVersion.set("version_sort", versionSort);
			commodityVersion.set("updateid", admin.get("id"));
			commodityVersion.set("updatename", admin.get("name"));
			commodityVersion.set("updatedate", new Date());
			commodityVersion.set("remarks", "修改商品");
			
			if(commodityVersion.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据id获取商品型号
	 */
	public void findCommodityVersionById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			CommodityVersion commodityVersion = CommodityVersion.dao.findById(id);
        	ret.put("versionType",  commodityVersion.getStr("version_type"));
        	ret.put("versionSort", commodityVersion.get("version_sort").toString());
        	ret.put("versionStock", commodityVersion.get("version_stock").toString());
        	ret.put("versionPrice", commodityVersion.get("version_price").toString());
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除商品型号
	 */
	@SuppressWarnings("null")
	public void delVersion(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_commodity_version where id = '"+id+"'");
				Db.update("delete from sta_commodity_version_pic where version_id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取商品型号图片列表
	 */
	public void getVersionPList(){
		String curPageStr = getPara("curPage");
		String versionId = getPara("versionId");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_commodity_version_pic  where version_id = '"+versionId+"'";
		
		try {
			
			Page<CommodityVersionPic> commodityVersionPicPage = CommodityVersionPic.dao.paginate(curPage, pageSize, "select *", sql+" order by pic_sort desc ,createdate desc");
	        List<Map<Object, Object>> commodityVersionPicList = new ArrayList<Map<Object, Object>>();
	        
	        for(CommodityVersionPic c:commodityVersionPicPage.getList()){
	        	Map<Object, Object> commodityVersionPic = new HashMap<Object, Object>();
	        	commodityVersionPic.put("id", c.getStr("id"));
	        	commodityVersionPic.put("picUrl",  c.getStr("pic_url"));
	        	commodityVersionPic.put("picSort", c.get("pic_sort").toString());
	        	commodityVersionPic.put("createName", c.get("createname").toString());
	        	commodityVersionPic.put("updateName", c.get("updatename").toString());
	        	commodityVersionPic.put("createDate", c.get("createdate").toString().substring(0,c.get("createdate").toString().length()-2));
	        	commodityVersionPic.put("updateDate", c.get("updatedate").toString());
	        	commodityVersionPicList.add(commodityVersionPic);
	        }
	        ret.put("data", commodityVersionPicList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(commodityVersionPicPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑图片排序
	 */
	public void updatePicSort(){
		String id = getPara("id");
		String picSort = getPara("picSort");
		RetDTO ret = new RetDTO();
		
		try {

			Administrator admin = getSessionAttr("admin");
			
			CommodityVersionPic commodityVersionPic = new CommodityVersionPic();
			commodityVersionPic.set("id", id);
			commodityVersionPic.set("pic_sort", picSort);
			commodityVersionPic.set("updateid", admin.get("id"));
			commodityVersionPic.set("updatename", admin.get("name"));
			commodityVersionPic.set("updatedate", new Date());
			commodityVersionPic.set("remarks", "修改图片排序");
			
			if(commodityVersionPic.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除图片
	 */
	@SuppressWarnings("null")
	public void delVersionPic(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_commodity_version_pic where id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 查看资讯
	 */
	public void showCommodityDesc(){
		String id = getPara("id");
		try {
			Commodity commodity = Commodity.dao.findById(id);
			setAttr("commodity", commodity);
			render("showcommoditydesc.jsp");
		} catch (Exception e) {
			System.out.print(e);
		}
		
	}
	
	

}
