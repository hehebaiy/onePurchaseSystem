/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.FeedBack;
import com.ideazd.system.model.NewDetails;
import com.ideazd.system.model.Propaganda;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.ideazd.system.util.UmengMsg;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/news",viewPath="/pages")
@Before(AdminInterceptor.class)
public class NewsDetailController extends Controller{
	
	/**
	 * 获取资讯列表
	 */
	public void getNewsDetailList(){
		String curPageStr = getPara("curPage");
		String title = getPara("title");
		String publishDate = getPara("publishDate");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_new_details   where 1 = 1";
        
        if(!StrKit.isBlank(title)){
        	sql += " and dettitle like '"+title+"'";
        }
        
        if(!StrKit.isBlank(publishDate)){
        	sql += " and createtime >'"+publishDate+"' and createtime < "+ "'"+publishDate.substring(0,10)+" 23:59:59'";
        }
		
		try {
			
			Page<NewDetails> NewDetailslPage = NewDetails.dao.paginate(curPage, pageSize, "select *", sql+" order by updatedate desc");
	        List<Map<String, String>> NewDetailstList = new ArrayList<Map<String, String>>();
	        
	        for(NewDetails n:NewDetailslPage.getList()){
	        	Map<String, String> NewDetails = new HashMap<String, String>();
	        	NewDetails.put("id", n.getStr("detid"));
	        	NewDetails.put("dettype", SysUtil.getDictionValue("sta_new_details", "dettype", n.get("dettype").toString()));
	        	NewDetails.put("dettitle", n.getStr("dettitle"));
	        	NewDetails.put("detpicture", n.getStr("detpicture"));
	        	NewDetails.put("detcontenturl", n.getStr("detcontenturl"));
	        	NewDetails.put("detcontent", n.getStr("detcontent"));
	        	NewDetails.put("topsource", n.getStr("topsource"));
	        	NewDetails.put("createtime", n.get("createdate").toString().substring(0,n.get("createdate").toString().length()-2));
	        	NewDetails.put("displayorder", n.get("displayorder").toString());
	        	NewDetails.put("displaystatus", n.get("displaystatus").toString());
	        	NewDetails.put("readamount", n.get("readamount").toString());
	        	NewDetails.put("createname", n.getStr("createname"));
	        	NewDetails.put("updatename", n.getStr("updatename"));
	        	NewDetailstList.add(NewDetails);
	        }
	        ret.put("data", NewDetailstList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(NewDetailslPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加资讯
	 */
	public void addNewsDetail(){
		File file = super.getFile("detpicture").getFile();
		
		
		//String ss = super.getFile("detpicture").getFileName();
		String detpicture = getPara("detpicture");
		//UploadFile upFile = getFile(getPara("detpicture"),"e:/");
		String dettype = getPara("dettype");
		String dettitle = getPara("dettitle");
		
		String detcontent = getPara("detcontent");
		String topsource = getPara("topsource");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		RetDTO ret = new RetDTO();
		System.out.println(getPara("dettype"));
		System.out.println(getPara("detpicture"));
		
		
		
		
		try {
			
			String uuid = SysUtil.getUUID();
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/newsdetail" + "/" + uuid + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				detpicture = imaPath.get("serverPath");

			}
			
			
			
			Administrator admin = getSessionAttr("admin");
			
			NewDetails newDetails = new NewDetails();
			newDetails.set("detid", uuid);
			newDetails.set("dettype", dettype);
			newDetails.set("dettitle", dettitle);
			newDetails.set("topsource", topsource);
			newDetails.set("detpicture", detpicture);
			
			
			
			newDetails.set("detcontenturl", getRequest().getScheme()+"://"+getRequest().getServerName()+":"+getRequest().getServerPort()+getRequest().getContextPath()+"/news/showNews?id="+uuid);
			newDetails.set("detcontent", detcontent);
			
			if(StrKit.isBlank(createtime)){
				newDetails.set("createtime", new Date());
			}else{
				newDetails.set("createtime", createtime);
			}
			
			newDetails.set("displayorder", displayorder);
			newDetails.set("displaystatus", displaystatus);
			newDetails.set("readamount", 0);
			newDetails.set("createid", admin.get("id"));
			newDetails.set("createname", admin.get("name"));
			newDetails.set("createdate", new Date());
			newDetails.set("updateid", admin.get("id"));
			newDetails.set("updatename", admin.get("name"));
			newDetails.set("updatedate", new Date());
			newDetails.set("remarks", "添加资讯");
			
			if(newDetails.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
		
		
	}
	
	/**
	 * 编辑资讯
	 */
	public void editNewsDetail(){
		File file = null;
		
		try {
			file = super.getFile("detpicture").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		String id = getPara("id");
		String dettype = getPara("dettype");
		String dettitle = getPara("dettitle");
		String detpicture = getPara("detpicture");
		String detcontent = getPara("detcontent");
		String topsource = getPara("topsource");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		RetDTO ret = new RetDTO();
		
		try {
			
			// 图片上传
			if (null != file) {

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),
						file,
						"/upload/topicpost" + "/" + id + "/images" + "/"
								+ DateUtil.formatDate(new Date(), "yyyyMMdd")
								+ "/");

				detpicture = imaPath.get("serverPath");

			}
			
			Administrator admin = getSessionAttr("admin");
			
			NewDetails newDetails = new NewDetails();
			newDetails.set("detid", id);
			newDetails.set("dettype", dettype);
			newDetails.set("dettitle", dettitle);
			newDetails.set("topsource", topsource);
			
			if(!StrKit.isBlank(detpicture)){
				newDetails.set("detpicture", detpicture);
			}
			
			//newDetails.set("detcontenturl", "");
			newDetails.set("detcontent", detcontent);
			if(StrKit.isBlank(createtime)){
				newDetails.set("createtime", new Date());
			}else{
				newDetails.set("createtime", createtime);
			}
			newDetails.set("displayorder", displayorder);
			newDetails.set("displaystatus", displaystatus);
			newDetails.set("updateid", admin.get("id"));
			newDetails.set("updatename", admin.get("name"));
			newDetails.set("updatedate", new Date());
			newDetails.set("remarks", "添加资讯");
			
			if(newDetails.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	//根据ID获资讯详细
	public void findNewsById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			NewDetails newDetails = NewDetails.dao.findById(id);
			ret.put("id", newDetails.getStr("detid"));
			ret.put("dettype", newDetails.get("dettype").toString());
			ret.put("dettitle", newDetails.getStr("dettitle"));
			ret.put("detpicture", newDetails.getStr("detpicture"));
			ret.put("detcontenturl", newDetails.getStr("detcontenturl"));
			ret.put("detcontent", newDetails.getStr("detcontent"));
			ret.put("topsource", newDetails.getStr("topsource"));
			ret.put("createtime", newDetails.get("createtime").toString());
			ret.put("displayorder", newDetails.get("displayorder").toString());
			ret.put("displaystatus", newDetails.getStr("displaystatus"));
			
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除界面排版
	 */
	@SuppressWarnings("null")
	public void delNewsDetail(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_new_details where detid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 启用/禁用
	 */
	public void handleDisplay(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			
			NewDetails newDetails = new NewDetails();
			newDetails.set("detid", id);
			newDetails.set("displaystatus", status);
			newDetails.set("updateid", admin.get("id"));
			newDetails.set("updatename", admin.get("name"));
			newDetails.set("updatedate", new Date());
			
			if(newDetails.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 查看资讯
	 */
	public void showNews(){
		String id = getPara("id");
		try {
			NewDetails newDetail = NewDetails.dao.findById(id);
			System.out.println(newDetail.get("createdate"));
			setAttr("newDetail", newDetail);
			render("newsshow.jsp");
		} catch (Exception e) {
			System.out.print(e);
		}
		
	}
	
	/**
	 * 推送
	 */
	public void sendNewsDetail(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			NewDetails newDetails =  NewDetails.dao.findById(id);
			String aSuccess = UmengMsg.androidSend(newDetails.getStr("dettitle"), newDetails.getStr("dettitle"), newDetails.getStr("detid"));
			String iSuccess = UmengMsg.iosSend(newDetails.getStr("dettitle"), newDetails.getStr("dettitle"), newDetails.getStr("detid"));
			
			if(aSuccess.indexOf("SUCCESS") > -1 && iSuccess.indexOf("SUCCESS") > -1){
				ret.setCode("0");
				ret.setMsg("发送成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	

}
