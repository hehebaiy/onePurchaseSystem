package com.ideazd.system.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ideazd.system.DefaultConfig;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.CommodityVersion;
import com.ideazd.system.model.CommodityVersionPic;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;
@ControllerBind(controllerKey="/FileUpload")
public class UploadController extends Controller {
	
	private static int imgCount=0;
	
	public void index(){
		render("upload.jsp");
	}
	
	
	public String imageUpload(String id){//返回文件存储相对路径
		Map<String, String> json = new HashMap<String, String>();
		UploadFile picString=getFile();
		DefaultConfig config = new DefaultConfig();
		
		if(picString!=null){
			String originalName=picString.getFileName();
			String extentionName=originalName.substring(originalName.lastIndexOf("."));	//后缀名
			
			if(imgCount>300)//300为文件上传最大数目
				imgCount=0;
			
			String newName=DateUtil.getCurrentTime()+"_"+imgCount+extentionName;//新名
			imgCount++;
			System.out.println(PathKit.getWebRootPath()+"//"+config.relativePath);
			String fileName=PathKit.getWebRootPath()+"//"+config.relativePath+"//"+newName;//文件完整路径
			picString.getFile().renameTo(new File(fileName)); //重命名并上传文件
			//获取文件相对路径
			//String projectName=PathKit.getWebRootPath().substring(PathKit.getWebRootPath().lastIndexOf('\\')+1);
			StringBuffer relativePath=new StringBuffer("//"+config.relativePath+"//"+newName);		
			for(int i=relativePath.indexOf("//");i!=-1;i=relativePath.indexOf("//"))
				relativePath.deleteCharAt(i);
			
			//插入数据库
			try {
				
				Administrator admin = getSessionAttr("admin");
				CommodityVersionPic pic = new CommodityVersionPic();
				pic.set("id", SysUtil.getUUID());
				pic.set("version_id", id);
				pic.set("pic_url", relativePath.toString());
				pic.set("pic_sort", "0");
				pic.set("createid", admin.get("id"));
				pic.set("createname", admin.get("name"));
				pic.set("createdate", new Date());
				pic.set("updateid", admin.get("id"));
				pic.set("updatename", admin.get("name"));
				pic.set("updatedate", new Date());
				pic.set("remarks", "上传图片");
				pic.save();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			}
			
			
			json.put("filePath", relativePath.toString());
			json.put("status", "success");
            
			
		}else{
			json.put("status", "error");
			//return null;
		}
		//renderJson(json);
		
		return json.toString();
		
	}
	
	public void upload(){
		//System.out.println(imageUpload());
		String id = getPara("id");
		//System.out.println(id);
		String json =  imageUpload(id);
		renderJson(json);
	}
}
