package com.ideazd.system.util;

import java.lang.reflect.Type;

import org.json.JSONObject;

import com.google.gson.Gson;
/** 
 * Java对象和JSON字符串相互转化工具类 
 * @author penghuaiyi 
 * @date 2013-08-10 
 */  
public final class JsonUtil {  
      
    private JsonUtil(){}  
      
    /** 
     * 对象转换成json字符串 
     * @param obj  
     * @return  
     */  
    public static String toJson(Object obj) {  
        Gson gson = new Gson();  
        return gson.toJson(obj);  
    }  
    
    /**
	 * 根据key获取给定的json数据的值
	 * 
	 * @param json
	 *            给定的JSON字符串
	 * @param key
	 *            指定的要获取值所对应的key
	 * @return 返回一个字符串，表示根据指定的key所得到的值，获取失败或发生JSON解析错误则返回空字符串
	 */
	public static String getJsonValueByKey(String json, String key) {
		String value = "";
		try {
			JSONObject jo = new JSONObject(json);
			value = jo.getString(key);
		} catch (Exception e) {
			value = "";
		}
		return value;
	}

  
    /** 
     * json字符串转成对象 
     * @param str   
     * @param type 
     * @return  
     */  
    public static <T> T fromJson(String str, Type type) {  
        Gson gson = new Gson();  
        return gson.fromJson(str, type);  
    }  
  
    /** 
     * json字符串转成对象 
     * @param str   
     * @param type  
     * @return  
     */  
    public static <T> T fromJson(String str, Class<T> type) {  
        Gson gson = new Gson();  
        return gson.fromJson(str, type);  
    }  
  
}  