/**
 * 
 */
package com.ideazd.system.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

/**
 * @author yzz
 * @create 2016年6月3日 下午5:35:24
 */
public class UmengMsg {
    private static String ioskey = "5754e447e0f55ad516002840";
    private static String iosSecret ="zlqh5dvtopzrzoxtv7biyv5robspixis";
    private static String androidKey = "5754e3ef67e58e38ad000bdb";
    private static String androidSecret ="zziil9yjd9sdmiu7s1g3pejigkp7tb4x";
	
	public static void sendMsg(){
		
	}
	
	public static String androidSend(String title,String content,String id){
        long timeStamp = new Date().getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("appKey", androidKey);
		jsonObject.put("timestamp", Long.toString(timeStamp));
		jsonObject.put("type", "broadcast");
		JSONObject body = new JSONObject();
		body.put("ticker", title);
		body.put("title", title);
		body.put("text", content);
		body.put("after_open", "go_custom");
		JSONObject custom = new JSONObject();
		custom.put("timestep", df.format(new Date()));
		custom.put("push_type", "1");
		JSONObject data = new JSONObject();
		data.put("newsDetailId", id);
		custom.put("data", data);
		body.put("custom", custom);
		JSONObject payload = new JSONObject();
		payload.put("display_type", "notification");
		payload.put("body", body);
		jsonObject.put("payload", payload);
		JSONObject alert = new JSONObject();
		alert.put("alert", title);
		jsonObject.put("aps", alert);
		System.out.println(jsonObject);
		return HttpClientUtil.getSign("http://msg.umeng.com/api/send", jsonObject.toString(), androidSecret);
   
	}
	
	public static String iosSend(String title,String content,String id){
        long timeStamp = new Date().getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("appKey", ioskey);
		jsonObject.put("timestamp", Long.toString(timeStamp));
		jsonObject.put("type", "broadcast");
		jsonObject.put("production_mode", "false");
		JSONObject body = new JSONObject();
		body.put("ticker", title);
		body.put("title", title);
		body.put("text", content);
		body.put("after_open", "go_activity");
		JSONObject custom = new JSONObject();
		custom.put("timestep", df.format(new Date()));
		custom.put("push_type", "1");
		JSONObject data = new JSONObject();
		data.put("newsDetailId", id);
		custom.put("data", data);
		body.put("custom", custom);
		JSONObject payload = new JSONObject();
		payload.put("display_type", "notification");
		JSONObject alert = new JSONObject();
		alert.put("alert",title);
		payload.put("aps", alert);
		payload.put("body", body);
		jsonObject.put("payload", payload);
		System.out.println(jsonObject);
		return HttpClientUtil.getSign("http://msg.umeng.com/api/send", jsonObject.toString(), iosSecret).toString();
   
	}
	public static void main(String[] args) {
		//AndroidSend("测试","测试","123");
		iosSend("测试","测试","123");
	}
	
	
}
