/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.ideazd.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import com.ideazd.system.controller.UploadController;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.tools.DateUtil;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Const;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.plugin.tablebind.SimpleNameStyles;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.ViewType;

import freemarker.template.TemplateModelException;

public class DefaultConfig extends JFinalConfig {
	public final static String relativePath="images//upload//"+DateUtil.getCurrentDate();
	public final static String filePath=PathKit.getWebRootPath()+"//"+relativePath;
	public final static Long startDate=new Date().getTime();
	public final static Map<String, List<String>> adminApplication = new HashMap<String, List<String>>();

	@Override
	public void afterJFinalStart() {
		super.afterJFinalStart();
		// 加载基础数据
		//CacheKit.put(cacheName, key, value);
		System.out.println(PathKit.getWebRootPath()+"//"+relativePath);
		
		
	}

	public Properties loadProp(String pro, String dev) {
		try {
			return loadPropertyFile(pro);
		} catch (Exception e) {
			return loadPropertyFile(dev);
		}
	}

	public void configConstant(Constants me) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		Properties props = loadProp("application_pro.properties", "application.properties");
        //设置开发模式
        me.setDevMode(getPropertyToBoolean("devMode", false));
        //设置视图类型为Jsp，否则默认为FreeMarker
        me.setViewType(ViewType.JSP);
        me.setUploadedFileSaveDirectory(PathKit.getWebRootPath()+"//"+relativePath);//文件上传保存路径
		me.setMaxPostSize(1*Const.DEFAULT_MAX_POST_SIZE);//上传文件最大为10M
		
	}

	public void configRoute(Routes me) {
		/*采用自动注解方式加载controller*/
		//me.add("/", WeixinFrontController.class, "/views");	//默认首页
		//me.add("/wx", WeixinApiController.class);				//微信菜单、关注数等
		//me.add("/msg", WeixinMsgController.class);			//微信消息等
		//自定控制层
		//me.add("/news", NewsController.class, "/views/news");
		//me.add("/care", CareLoveController.class, "/views/love");
		//me.add("/pay", WeixinPayController.class, "/pay"); 	//支付
		//me.add("/", UploadController.class);
		/* 自动扫描 [注解方式] */
		AutoBindRoutes autoBindRoutes = new AutoBindRoutes();
		List excludeClasses = new ArrayList();
//		excludeClasses.add(WeixinApiController.class);
//		excludeClasses.add(WeixinMsgController.class);
		autoBindRoutes.addExcludeClasses(excludeClasses);
		me.add(autoBindRoutes);
	}

	public void configPlugin(Plugins me) {
		/* 启用数据库连接 */
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);

		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		
		
		
		/*非注解方式
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setShowSql(true);
		me.add(arp);
		
		arp.addMapping("cwo_news_content", "content_id", News.class); // 映射cwo_news_content 表到 News模型
		arp.addMapping("cwo_news_column", "news_id", Catalog.class); // 映射cwo_news_column 表到 Catalog模型
		arp.addMapping("cwo_wx_plan", "plan_id", Plan.class); //
		arp.addMapping("cwo_wx_plan_join", "join_id", PlanJoin.class); // 
		arp.addMapping("cwo_wx_user", "user_id", User.class); // 
		arp.addMapping("cwo_wx_user_amount", "amount_id", UserAmount.class); // 
		*/
		
		//开启注解
		/*enable*/
		AutoTableBindPlugin atbp = new AutoTableBindPlugin(c3p0Plugin, SimpleNameStyles.LOWER);
		atbp.setShowSql(true);
		me.add(atbp);
		
		
	}

	public void configInterceptor(Interceptors me) {
		//me.add(new AdminInterceptor());
		me.addGlobalServiceInterceptor(new AdminInterceptor());

	}

	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctx"));
		//添加全局变量另一种写法
		/*try {
			FreeMarkerRender.getConfiguration().setSharedVariable("v", System.currentTimeMillis());
		} catch (TemplateModelException e) {
			e.printStackTrace();
		}*/
	}

	public static void main(String[] args) {
		JFinal.start("webapp", 80, "/", 5);
	}
}
