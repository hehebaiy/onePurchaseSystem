/**
 * 
 */
package com.ideazd.system.model;

import java.util.List;

/**
 * @author yzz
 * @create 2016年5月23日 下午3:21:34
 */
public class GetPermission {
	private String permissionName;
	private String ioc;
	private List<Permission> permission;
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public String getIoc() {
		return ioc;
	}
	public void setIoc(String ioc) {
		this.ioc = ioc;
	}
	public List<Permission> getPermission() {
		return permission;
	}
	public void setPermission(List<Permission> permission) {
		this.permission = permission;
	}
	
	
	
	

}
