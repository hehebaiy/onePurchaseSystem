<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<script type="text/javascript">
  
</script>
<title>商品型号</title>
</head>
<body>
<nav class="breadcrumb"> 商品型号 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a></span>
  &nbsp;<button type="submit" class="btn btn-primary radius"  onclick="commodity_version_add('添加型号','<%=basePath%>commodityVersionAdd','0001','600','600')" id="addDiction" name="addDiction"><i class="Hui-iconfont">&#xe600;</i> 添加型号</button>  </div>
  <div class="mt-20">
    <table class="table table-border table-bordered table-bg table-hover table-sort">
    <thead>
      <tr class="text-c">
        <th width="25"><input type="checkbox" name="" value=""></th>
        <th width="105">型号</th>
        <th width="105">价格</th>
        <th width="100">库存量</th>
        <th width="100">排序</th>
        <th width="100">操作</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
</div>
 <%@include file="../common/js.jsp" %>
<script type="text/javascript" src="<%=basePath%>/static/js/commodity-version-list.js"></script>


</script> 
</body>
</html>
