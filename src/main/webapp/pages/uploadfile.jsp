<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=basePath%>/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />
<title>图片上传</title>
</head>
<body>
<div class="row cl">
	<div class="formControls col-10">
		<div class="uploader-list-container">
		
			<div class="queueList">
				<div id="dndArea" class="placeholder">
					<div id="filePicker-2"></div>
					<p>或将照片拖到这里，单次最多可选300张</p>
				</div>
			</div>
			
			<div class="statusBar" style="display:none;">
				<div class="progress"> <span class="text">0%</span> <span class="percentage"></span> </div>
				<div class="info"></div>
				<div class="btns">
					<div id="filePicker2"></div>
					<div class="uploadBtn">开始上传</div>
				</div>
			</div>
			
		</div>
	</div>
</div>


<script type="text/javascript" src="<%=basePath%>/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="<%=basePath%>/lib/webuploader/0.1.5/webuploader.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/fileUpload.js"></script> 
<script type="text/javascript" src="<%=basePath%>/static/js/common.js"></script> 
<script type="text/javascript">
(function( $ ){
    // 当domReady的时候开始初始化
    var id = getQueryString("id");
   // alert(id);
    $(function() {
    	
    	uploadReady(basePath+'/FileUpload/upload/?id='+id+'');
    });
    
})( jQuery );

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
}
</script>
</body>
</html>