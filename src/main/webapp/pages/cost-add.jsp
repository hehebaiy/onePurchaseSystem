<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<%@include file="../common/css.jsp" %>
<title>添加运费</title>
</head>
<body>
	<article class="page-container aform">
	    <form class="form form-horizontal" id="form-admin-add" >
			<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>区域：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="provinceCode"  id="provinceCode" class="select">
					<option value="">请选择资讯类型</option>
				</select>
				</span> </div>
		    </div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>运费：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder="" id="cost" name="cost">
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>
</body>
</html>
<%@include file="../common/edit-js.jsp" %> 
<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript">
$(document).ready(function(){	
	var id = getQueryString('id');
	$.validator.addMethod("existValue",function(value,element){
		var flag = false;
		$.ajax({
			type:"post",
			url:"../cost/provinceValidate",
			data:{"id":id,"province":value},
			async:false,
			success:function(data){
				 if(data.code == "0"){
					 flag =  true;
			        }
			}
		});
		return flag;
	},"该区域已存在");
	
	//获取省
	$.ajax({
			type:"post",
			url:"../cost/getProvice",
			async:false,
			success:function(data){
				 for(var i = 0; i < data.data.length; i++){
					 $("#provinceCode").append("<option value='"+data.data[i].areaCode+"'>"+data.data[i].areaName+"</option>");
				 }
			}
		});
	
	var url = "../cost/addCost";
	
	if(id != null){
		url = "../cost/editCost?id="+id+"";
		
		
		$.ajax({
			type:"post",
			url:"../cost/getCostById",
			data:{"id":id},
			async:false,
			success:function(data){
				$('#provinceCode').val(data.data.provinceCode);
				$('#cost').val(data.data.cost);
			}
		});
		
	}
	
	$(".form-horizontal").validate({
		rules:{
			provinceCode:{
				required:true,
				maxlength:10,
				existValue:true
			},
			cost:{
				required:true,
				maxlength:30
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
		        	 },1000)
		        	 
		        	
		        }
		      });
		}
	});
	
});

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>