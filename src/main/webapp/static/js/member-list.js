var curPage = 1;
var loginDate = "";
var nickName = "";

$(function(){
	
	//加载
	getUserList(curPage,loginDate,nickName);
	//搜索
	$("body").on("click","#search",function(){
		loginDate = $('#loginDate').val();
		nickName = $('#nickName').val();
		curPage =1;
		getUserList(curPage,loginDate,nickName);
	})
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getUserList($(this).data('page'),loginDate,nickName);
	});
	
});

var getUserList = function(curPage,loginDate,nickName){
	var html="";
	$.post(basePath+"/user/getUserList",{"curPage":curPage,"loginDate":loginDate,"nickName":nickName},function(data){
		for(var i = 0; i < data.data.length; i++){
			var isenable='<span class="label radius">已停用</span>';
			var bBtn = '<a style="text-decoration:none" onclick="member_start(this,\''+data.data[i].id+'\')" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe615;</i></a>';
			if(data.data[i].userstatus == "0"){
				isenable = '<span class="label label-success radius">已启用</span>';
				bBtn = '<a style="text-decoration:none" onclick="member_stop(this,\''+data.data[i].id+'\')" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>';
			}
			var imgtd="";
			
			if(isEmpty(data.data[i].headportrait) == ""){
				imgtd=basePath+'/static/h-ui/images/user.png';
			}else{
				imgtd = data.data[i].headportrait;
			}
			
			var userType = "&#xe696;";
			
			if(data.data[i].usetype == "微信"){
				userType = "&#xe694;";
			}else if(data.data[i].usetype == "QQ"){
				userType = "&#xe67b;";
			}
			
			html+='<tr class="text-c">'+
			'<td><input type="checkbox" value="1" data-id=\''+data.data[i].id+'\'></td>'+
			//'<td>'+data.data[i].account+'</td>'+
			'<td>'+data.data[i].nickname+'</td>'+
			'<td>'+data.data[i].gender+'</td>'+
			'<td><img style="width:50px;height:50px;" src="'+imgtd+'"  alt="'+data.data[i].nickname+'" /></td>'+
			'<td>'+data.data[i].phone+'</td>'+
			'<td><i class="Hui-iconfont" style="font-size:30px">'+userType+'</i></td>'+
			'<td>'+data.data[i].logintime+'</td>'+
			'<td>'+data.data[i].lastlogin+'</td>'+
			'<td class="td-status">'+isenable+'</td>'+
			'<td class="td-manage">'+bBtn+'<a title="编辑" href="javascript:;" onclick="member_edit(\'用户编辑\',\'admin-add.jsp?id='+data.data[i].id+'\',\'1\',\'800\',\'500\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a></td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
		}
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}


/*用户-添加*/
function member_add(title,url,w,h){
	layer_show(title,url,w,h);
}
/*用户-查看*/
function member_show(title,url,id,w,h){
	layer_show(title,url,w,h);
}
/*用户-停用*/
function member_stop(obj,id){
	layer.confirm('确认要停用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/user/stopAndEnableUser",
			data:{"id":id,"status":"1"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="member_start(this,\''+id+'\')" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">已禁用</span>');
					$(obj).remove();
					layer.msg('已停用!',{icon: 5,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

/*用户-启用*/
function member_start(obj,id){
	layer.confirm('确认要启用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/user/stopAndEnableUser",
			data:{"id":id,"status":"0"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="member_stop(this,\''+id+'\')" href="javascript:;" title="停用" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
					$(obj).remove();
					layer.msg('已启用!', {icon: 6,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}
/*用户-编辑*/
function member_edit(title,url,id,w,h){
	layer_show(title,url,w,h);
}
/*密码-修改*/
function change_password(title,url,id,w,h){
	layer_show(title,url,w,h);	
}
/*用户-删除*/
function member_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		$(obj).parents("tr").remove();
		layer.msg('已删除!',{icon:1,time:1000});
	});
}