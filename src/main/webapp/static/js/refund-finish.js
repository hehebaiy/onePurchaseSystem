
var url = basePath+"/basic/addPropaganda";
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	 //alert($('#newsrefresh').attr('href'));
	$list = $("#fileList"),
	$btn = $("#btn-star"),
	$.ajaxSetup({   
        async : false  
    }); 
	
	//事件跳转
	$("#operationevent").change(function(){
		if($(this).children('option:selected').val() == 1){
			$("#eventinfo").attr("disabled",true);
			$("#h5event").attr("disabled",false);
		}else if($(this).children('option:selected').val() == 2){
			$("#eventinfo").attr("disabled",false);
			$("#h5event").attr("disabled",true);
			
			$.post(basePath+"/basic/getTopicList",function(data){
				$("#eventinfo").html("");
				 for(var i = 0; i < data.data.length; i++){
					 $("#eventinfo").append("<option value='"+data.data[i].id+"'>"+data.data[i].toptitle+"</option>");
				 }
			});
			
		}else if($(this).children('option:selected').val() == 3){
			$("#eventinfo").attr("disabled",false);
			$("#h5event").attr("disabled",true);
			
			$.post(basePath+"/basic/getNewsList",function(data){
				$("#eventinfo").html("");
				 for(var i = 0; i < data.data.length; i++){
					 $("#eventinfo").append("<option value='"+data.data[i].id+"'>"+data.data[i].dettitle+"</option>");
				 }
			});
			
		}
	});
	
	var id = getQueryString('id');
	
	if(id != null ){
		$.post(basePath+"/basic/findPropagandaById",{"id":id},function(data){
			 $('#picposition').val(data.data.picposition);
			 $('#displaystatus').val(data.data.displaystatus);
			 $('#displayorder').val(data.data.displayorder);
			 $('#operationevent').val(data.data.operationevent);
			 
			 if(data.data.operationevent == "1"){
				 $('#h5event').val(data.data.eventinfo);
				 $("#eventinfo").attr("disabled",true);
				$("#h5event").attr("disabled",false);
			 }else{
				 
				 if(data.data.operationevent == "2"){
					 $.post(basePath+"/basic/getTopicList",function(data){
							$("#eventinfo").html("");
							 for(var i = 0; i < data.data.length; i++){
								 $("#eventinfo").append("<option value='"+data.data[i].id+"'>"+data.data[i].toptitle+"</option>");
							 }
						});
				 }else if(data.data.operationevent == "3"){
					 $.post(basePath+"/basic/getNewsList",function(data){
							$("#eventinfo").html("");
							 for(var i = 0; i < data.data.length; i++){
								 $("#eventinfo").append("<option value='"+data.data[i].id+"'>"+data.data[i].dettitle+"</option>");
							 }
						});
				 }
				 
				 $('#eventinfo').val(data.data.eventinfo);
				 $("#eventinfo").attr("disabled",false);
				 $("#h5event").attr("disabled",true);
			 }
			 
			 
			 
			 $('#listPicture').attr("src",basePath+'/'+data.data.picaddress);
			 $('#listPicture').val(data.data.detpicture);
			 url = basePath+"/basic/editPropaganda?id="+id+"";
		});
	}
	
	
	
	//验证
	$('#subBtn').on('click',function(){
		//alert($('#dettype').val());
		if($('#picposition').val() == ""){
			layer.msg("请选择宣传位置",{time:1000});
			return false;
		}
		if($('#pictrure').val() == "" && ('#listPicture').attr('src') == ""){
			layer.msg("请上传图片",{time:1000});
			return false;
		}
		
		if($('#operationevent').val() == ""){
			layer.msg("请选择跳转类型",{time:1000});
			return false;
		}
		
		if($('#operationevent').val() == "1"){
			if($('#h5event').val()==""){
				ayer.msg("请填写跳转的H5地址",{time:1000});
				return false;
			}
		}
		
	});
	
	fromValidate($('.form'));
	
	initFileUpload();
	
});

//编辑时，初始化上传图片的插件，显示移除、修改按钮，隐藏选择图片按钮
var initFileUpload = function(){
	//获取图片的地址
	var picUrl = $("#listPicture").attr("src");
	
	//如果存在图片，则进行移除、修改的显示，选择图片的隐藏
	if("" != picUrl){
		//预览图片的显示
		$(".fileupload-preview").show();
		//移除、修改的显示，选择图片的隐藏
		$(".fileupload").addClass('fileupload-exists').removeClass('fileupload-new');
	}
	
	
}


var fromValidate = function(form){

	
	
	$(form).validate({
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 