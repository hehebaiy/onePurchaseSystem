function getRootPath_web() {
            //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
            var curWwwPath = window.document.location.href;
            //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
            var pathName = window.document.location.pathname;
            var pos = curWwwPath.indexOf(pathName);
            //获取主机地址，如： http://localhost:8083
            var localhostPaht = curWwwPath.substring(0, pos);
            //获取带"/"的项目名，如：/uimcardprj
            var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
            return (localhostPaht + projectName);
        }

basePath = getRootPath_web();

function isEmpty(str){
	if(!str){
		return "";
	}else{
		return str;
	}
}

$(function(){
	$("ul li a").on("click",function(){
		//alert(seesionIsExist("admin"));
		if(seesionIsExist("admin") == "-3") {
			layer.msg("用户登录超时，请重新登录",{time:1000});
	   		 setTimeout(function(){
	   			 window.parent.parent.location.href=basePath+"/login";	 
	       	 },2000)
	       	return false;  	 
		}
	})
});

function removeIframe(){
	var index = parent.layer.getFrameIndex(window.name);
	 parent.layer.close(index);
}

/**
 * 获取session
 */
function seesionIsExist(sessionName){
	$.ajaxSetup({   
        async : false  
    });
	var code = "";
	$.post(basePath+"/sys/seesionIsExist",{"sessionName":sessionName},function(data){
		//alert( data.code);
		code =  data.code;
	});
	return code;
}

/**
 * 防止用户直接关闭窗口

$(window).bind('beforeunload',function(){  

	  $.ajax({  

	    url:basePath+"/admin/adminLoginOut?outType=wind",  

	    type:"post",  

	    success:function(){  

	        //alert("您已退出登录");  

	    }  

	});  

});   */
