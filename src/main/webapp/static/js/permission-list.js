var curPage = 1;
$(function(){
	
	getPermissionList(curPage);
	
	
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getPermissionList($(this).data('page'));
	});
});

var getPermissionList = function(curPage,datemin,datemax,name){
	var html="";
	$.post(basePath+"/per/getPermissionList",{"curPage":curPage},function(data){
		for(var i = 0; i < data.data.length; i++){
			
			html+='<tr class="text-c">'+
			'<td><input type="checkbox" value="1" data-id=\''+data.data[i].id+'\'></td>'+
			'<td>'+data.data[i].permissionname+'</td>'+
			'<td><i class="Hui-iconfont">'+data.data[i].ioc+'</i></td>'+
			'<td>'+data.data[i].createdate+'</td>'+
			'<td class="td-manage"><a title="编辑" href="javascript:;" onclick="permission_add(\'编辑权限\',\''+basePath+'/addPermission?id='+data.data[i].id+'\',\'500\',\'300\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a><a title="查看子权限" href="javascript:;" onclick="permission_add(\'添加权限\',\''+basePath+'/getChildPermission?id='+data.data[i].id+'\',\'800\',\'800\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe665;</i></a> <a title="删除" href="javascript:;" onclick="admin_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
		}
		
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}

/*管理员-增加*/
function permission_add(title,url,w,h){
	layer_show(title,url,w,h);
}
/*管理员-删除*/
function admin_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		
		$.ajax({
			type:"post",
			url:basePath+"/per/delPermission",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}


/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/per/delPermission",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getPermissionList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}