$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	$.Huitab("#tab-system .tabBar span","#tab-system .tabCon","current","click","0");
	
	$.post(basePath+"/sys/getSystemConfig",function(data){
    	$('#sysName').val(data.data.systemname);
    	$('#footright').val(data.data.footright);
    	$('#discription').val(data.data.discription);
    	$('#uploadurl').val(data.data.uploadurl);
    	$('#recordnumber').val(data.data.recordnumber);
    });
	
	$("#form-article-add").validate({
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:basePath+"/sys/updateSystemConfig",
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	 layer.msg(data.msg, {
						    time: 2000 //2s后自动关闭
						  });
		        	 parent.location.reload();
		        }
		      });
		},
        rules: {
        	sysName: {
			    required: true,
			    maxlength: 50
		    },
		    discription: {
			    required: true,
			    maxlength: 160
		    },
		    uploadurl: {
			    required: true,
			    maxlength: 50
		    },
		    footright: {
			    required: true,
			    maxlength: 50
		    },
		    recordnumber: {
			    required: true,
			    maxlength: 50
		    },
	        
       },
        messages: {
        	sysName: {
				    required: "请输入系统名称",
				    maxlength: jQuery.format("系统名称不能大于{0}个字 符")
			 },
			 discription: {
				    required: "请输入系统描述",
				    maxlength: jQuery.format("系统描述不能大于{0}个字 符")
			 },
			 uploadurl: {
				    required: "请输上传目录配置",
				    maxlength: jQuery.format("上传目录配置不能大于{0}个字 符")
			 },
			 footright: {
				    required: "请输底部版权信息",
				    maxlength: jQuery.format("部版权信息不能大于{0}个字 符")
			 },
			 recordnumber: {
				    required: "请输入备案号",
				    maxlength: jQuery.format("备案号不能大于{0}个字 符")
			 },
       }
    });
});