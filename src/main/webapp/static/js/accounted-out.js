var curPage = 1;
$(function(){
	   
	   //加载表格数据
	   getAccountOutList(curPage);
	  //分页操作
		$("body").on("click",".paginate_button",function(){
			getAccountOutList($(this).data('page'));
		});
		
	   
});


//分页加载
var getAccountOutList = function(curPage){
	$.post(basePath+"/account/getAccountedOutList",{"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
    	
		 for(var i=0;i<data.data.length;i++){
			
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].nickname+'</td>'+
			        '<td>'+data.data[i].ordnumber+'</td>'+
			        '<td>'+data.data[i].outtype+'</td>'+
			        '<td>'+data.data[i].outtime+'</td>'+
			        '<td>'+data.data[i].outamount+'</td>'+
			        '<td>'+data.data[i].accountname+'</td>'+
			        '<td>'+data.data[i].bankaccount+'</td>'+
			        '<td>'+data.data[i].cardnumber+'</td>'+
			        '<td>'+data.data[i].outremarks+'</td>'+
			        '<td>'+data.data[i].updatename+'</td>'+
			     ' </tr>';
			  
		  }
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 $('tbody').html(html);
		 $('#DataTables_Table_0_paginate').html(data.pages);
	   });
}

