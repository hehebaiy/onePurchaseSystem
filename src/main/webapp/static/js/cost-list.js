var curPage = 1;
$(function(){
	   //加载表格数据
	    getDictionList(curPage);
	  //分页操作
		$("body").on("click",".paginate_button",function(){
			getDictionList($(this).data('page'));
		});
		//默认运费
		$.post(basePath+"/cost/getDefaultCost",function(data){
			
			$('#defaultCost').html(data.data.cost);
		});
		
		$("body").on("click","#defaultCost",function(){
			if($(this).html().indexOf("input")==-1){
				var varvalue=$(this).html();
				
				$(this).html('<input type="text" class="input-text" onblur="edit()" style="width:250px" value="'+varvalue+'">');
				$(".input-text").focus();
			}
		});
	   
});


function edit(){
	var cost = $(".input-text").val();	
	if(cost == ""||cost == null){
		layer.msg("请填默认运费",{time:1000});
		$(".input-text").s();
		return false;
	}else{
		$.ajax({
			type:"post",
			url:basePath+"/cost/editDefaultCost",
			data:{"cost":cost},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$("#defaultCost").html(cost);
					layer.msg('修改成功!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
	}
	
}

//分页加载
var getDictionList = function(curPage){
	$.post(basePath+"/cost/getCostList",{"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
		 for(var i=0;i<data.data.length;i++){
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].areaName+'</td>'+
			        '<td>'+data.data[i].cost+'</td>'+
			        '<td>'+data.data[i].createTime+'</td>'+
			        '<td>'+data.data[i].createName+'</td>'+
			        '<td>'+data.data[i].updateDate+'</td>'+
			        '<td>'+data.data[i].updateName+'</td>'+
			        '<td class="f-14"><a style="text-decoration:none" onclick="cost_add(\'运费编辑\',\''+basePath+'/costAdd?id='+data.data[i].id+'\',\'0001\',\'500\',\'300\')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="cost_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>'+
			     ' </tr>';
			  
		  }
		 
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 $('tbody').html(html);
		 $('#DataTables_Table_0_paginate').html(data.pages);
	   });
}


/*添加运费*/
function cost_add(title,url,id,w,h){
  layer_show(title,url,w,h);
}
/*运费*/
function cost_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/cost/delCost",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/sys/delDictionary",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getDictionList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}