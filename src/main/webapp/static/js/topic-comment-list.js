var dictionaryId = "";
var curPage = 1;
var url = basePath+"/topic/getTopicCommentList";
$(function(){
	
	topicId = getQueryString('topicId');
	
	if(topicId == null){
		url = basePath+"/topic/getCommentList";
	}
	
	//alert(dictionaryId);
	
	   //加载表格数据
	getTopicCommentList(curPage);
    //分页操作
	$("body").on("click",".paginate_button",function(){
		getTopicCommentList($(this).data('page'));
	});
	   
});

var getTopicCommentList = function(curPage){
	$.post(url,{"topicId":topicId,"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
		 for(var i=0;i<data.data.length;i++){
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox" data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].nickname+'</td>'+
			        '<td>'+data.data[i].comcontent+'</td>'+
			        '<td>'+data.data[i].createtime+'</td>'+
			        '<td class="f-14"><a title="删除" href="javascript:;" onclick="comment_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>'+
			     ' </tr>';
			  
		  }
		 
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
			}
			
			$('tbody').html(html);
			$('#DataTables_Table_0_paginate').html(data.pages);
	   });
}




/*数据字典-删除*/
function comment_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/topic/delTopicComment",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/topic/delTopicComment",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getTopicCommentList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}