var curPage = 1;
var handleState = "";
var submitDate = "";
$(function(){
	
	getdbackList(curPage,handleState,submitDate);
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getdbackList($(this).data('page'),handleState,submitDate);
	});
	
	//加载处理类型
	$.post(basePath+"/sys/findDictionByTablenameAndTableField",{"tableName":"sta_feedback","tableField":"handlestate"},function(data){
		 for(var i = 0; i < data.data.length; i++){
			 $("#handleState").append("<option value='"+data.data[i].fieldvalue+"'>"+data.data[i].displayvalue+"</option>");
		 }
	});
	
	//搜索
	$("body").on("click","#search",function(){
		handleState = $('#handleState').val();
		submitDate = $('#submitDate').val();
		curPage =1;
		getdbackList(curPage,handleState,submitDate);
	})
});

var getdbackList = function(curPage,handleState,submitDate){
	var html="";
	$.post(basePath+"/basic/getFeedBackList",{"curPage":curPage,"handleState":handleState,"submitDate":submitDate},function(data){
		for(var i = 0; i < data.data.length; i++){
			
			var isHandle='<span class="label radius">已处理</span>';
			var bBtn = '<a style="text-decoration:none" href="javascript:;" title="完成处理"><i class="Hui-iconfont">&#xe615;</i></a>';
			if(data.data[i].handlestate == "0"){
				isHandle = '<span class="label label-success radius">未处理</span>';
				bBtn = '<a style="text-decoration:none" onclick="handleFeedBack(this,\''+data.data[i].id+'\')" href="javascript:;" title="完成处理"><i class="Hui-iconfont">&#xe631;</i></a>';
			}
			
			html+='<tr class="text-c">'+
			'<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			'<td>'+data.data[i].nickname+'</td>'+
			'<td>'+data.data[i].contactmode+'</td>'+
			'<td>'+data.data[i].feeinfo+'</td>'+
			'<td>'+data.data[i].createtime+'</td>'+
			'<td class="td-status">'+isHandle+'</td>'+
			'<td class="td-manage">'+bBtn+'</td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
		}
		
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}



/*处理*/
function handleFeedBack(obj,id){
	layer.confirm('确认该反馈已处理吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/basic/handleFeedBack",
			data:{"id":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" href="javascript:;" title="完成处理"><i class="Hui-iconfont">&#xe615;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label radius">已处理</span>');
					$(obj).remove();
					layer.msg('处理成功!',{icon: 5,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

